Rails.application.routes.draw do
  namespace :api, defaults: {format: 'json'} do
    namespace :v1 do
      resources :albums do 
        resources :artists do
          resources :tracks
        end
        resources :tracks
      end
      resources :artists do
        resources :albums do 
          resources :tracks
        end
        resources :tracks
      end
    end
  end
  resources :artists do
    resources :albums
  end
  root 'artists#index'
end

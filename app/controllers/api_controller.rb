class ApiController < ActionController::Base
  	# Prevent CSRF attacks by raising an exception.
  	# For APIs, you may want to use :null_session instead.
  	protect_from_forgery with: :null_session
  
  	respond_to :json
  	responders :json

  	rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found

  	def generic_response(data, status = 200)
        @response = {:data => data, :meta => {:code => status}}

        if params[:with]
        	#@response = @response.to_json(include: params[:with])
          @response = @response.to_json(include: params[:with].split(','))

        end
        
        respond_with(@response, :status => status,:location => nil)
    end

	def record_not_found
		render json: {
			meta: {
				code: 404,
				error_type: 'RecordError',
				error_message: 'Record Not Found'
			},
			data: []
		}, status: 404
	end
end

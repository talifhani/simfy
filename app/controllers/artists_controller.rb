class ArtistsController < ApplicationController
  def index
    response = RestClient.get @api_url + 'artists'
    response = JSON.parse(response)
    @artists = response["data"]
  end

  def create
    #logger.info "_____________________________________: " + pa.inspect
    response = RestClient.post @api_url + "artists", artist_params.to_json, :content_type => :json, :accept => :json
    response = JSON.parse(response)
    @artist = response['data']
    logger.debug "@artist : " + @artist.inspect

    redirect_to artists_path
  end

  def update
    @artist = Artist.find(params[:id])

    @artist.update_attributes(write_params)

    generic_response @artist
  end

  def new
    response = RestClient.get @api_url + 'artists'
    response = JSON.parse(response)
    @artists = response['data']
  end


  def destroy
    @artist.destroy
    @response = {:data => [], :meta => {:code => 200}}
    respond_with @response
  end

  def show
    response = RestClient.get @api_url + 'artists/' + params[:id] + '?with=albums'
    response = JSON.parse(response)
    @artist = response["data"][0]
  end

  private
  ## Strong Parameters 
  def artist_params
    params.permit(:title)
  end

  def filter_params
    params.permit(:title, :id)
  end
end

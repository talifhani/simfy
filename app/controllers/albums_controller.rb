class AlbumsController < ApplicationController
  def index
    artist = params[:artist_id] ? 'artists/' + params[:artist_id] + '/' : ''
    response = RestClient.get @api_url + artist + 'albums?with=artist'
    response = JSON.parse(response)
    @albums = response["data"]
  end

  def create
    #logger.info "_____________________________________: " + pa.inspect
    response = RestClient.post @api_url + "albums", album_params.to_json, :content_type => :json, :accept => :json
    response = JSON.parse(response)
    @album = response['data']
    logger.debug "@album : " + @album.inspect
    if(response["meta"]["code"] == 201) 
      params[:tracks].each do |track|
        track["artist_id"] = @album["artist_id"]
        response2 = RestClient.post @api_url + "albums/" + @album["id"].to_s + "/tracks", track.to_json, :content_type => :json, :accept => :json
      end      
    end
    
    redirect_to artist_album_path(@album['artist_id'], @album['id'])
    
  end

  def update
    @artist = Artist.find(params[:id])

    @artist.update_attributes(write_params)

    generic_response @artist
  end

  def new
    response = RestClient.get @api_url + 'artists'
    response = JSON.parse(response)
    @artists = response['data']

    response = RestClient.get @api_url + 'artists/' + params[:artist_id]
    response = JSON.parse(response)
    @artist = response['data'][0]
  end


  def destroy
    @artist.destroy
    @response = {:data => [], :meta => {:code => 200}}
    respond_with @response
  end

  def show
    response = RestClient.get @api_url + 'albums/' + params[:id] + '?with=tracks,artist'
    response = JSON.parse(response)
    @album = response["data"][0]
  end

  private
  ## Strong Parameters 
  def album_params
    params.permit(:title, :year, :artist_id)
  end

  def track_params
    params.permit(:title, :artist_id)
  end

  def filter_params
    params.permit(:title, :id)
  end
end

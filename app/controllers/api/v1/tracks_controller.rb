module Api
  module V1
    class TracksController < ApiController
      def index
        @data = Track.where(filter_params).all
        generic_response @data
      end

      def create
        @track = Track.new(write_params)

        @album = Album.find(params[:album_id])

        @album.tracks << @track

        if @album.save
          generic_response @track, 201
        else
          generic_response [], 400
        end
      end

      def update
        @track = Track.find(params[:id])

        @track.update_attributes(write_params)

        generic_response @track
      end

      def new
      end


      def destroy
        @track = Track.find(params[:id])
        @track.destroy
        generic_response []
      end

      def show
        index
      end

      private
      ## Strong Parameters 
      def write_params
        params.permit(:title, :year, :artist_id)
      end

      def filter_params
        params.permit(:title, :id, :year, :artist_id)
      end
    end
  end
end
module Api
  module V1
    class AlbumsController < ApiController
      def index
        @data = Album.where(filter_params).all
        generic_response @data
      end

      def create
        @album = Album.new(write_params)

        if @album.save
          generic_response @album, 201
        else
          generic_response [], 400
        end
      end

      def update
        @album = Album.find(params[:id])

        @album.update_attributes(write_params)

        generic_response @album
      end

      def new
      end


      def destroy
        @album = Album.find(params[:id])
        @album.destroy
        generic_response []
      end

      def show
        index
      end

      private
      ## Strong Parameters 
      def write_params
        params.permit(:title, :year, :artist_id)
      end

      def filter_params
        params.permit(:title, :id, :year, :artist_id)
      end
    end
  end
end
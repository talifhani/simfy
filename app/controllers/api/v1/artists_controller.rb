module Api
  module V1
    class ArtistsController < ApiController
      def index
        @data = Artist.where(filter_params).all
        generic_response @data
      end

      def create
        @artist = Artist.new(write_params)

        if @artist.save
          generic_response @artist, 201
        else
          generic_response [], 400
        end
      end

      def update
        @artist = Artist.find(params[:id])

        @artist.update_attributes(write_params)

        generic_response @artist
      end

      def new
      end


      def destroy
        @artist.destroy
        @response = {:data => [], :meta => {:code => 200}}
        respond_with @response
      end

      def show
        index
      end

      private
      ## Strong Parameters 
      def write_params
        params.permit(:title)
      end

      def filter_params
        params.permit(:title, :id)
      end
    end
  end
end
class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  
  def initialize()
  	super
  	@api_url = 'http://localhost:3002/api/v1/';	
  end
end

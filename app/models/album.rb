class Album < ActiveRecord::Base
	has_and_belongs_to_many :tracks
	belongs_to :artist
end

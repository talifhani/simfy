class Track < ActiveRecord::Base
	has_and_belongs_to_many :albums
	belongs_to :artist
end

class CreateAlbumsTracks < ActiveRecord::Migration
  def change
    create_table :albums_tracks, id: false do |t|
		t.belongs_to :album
      	t.belongs_to :track
    end
    add_index :albums_tracks, [:album_id, :track_id]
  end
end

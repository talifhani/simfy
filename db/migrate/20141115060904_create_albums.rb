class CreateAlbums < ActiveRecord::Migration
  def change
    create_table :albums do |t|
      t.string :title
      t.integer :artist_id
      t.integer :year

      t.timestamps
    end
    add_index :albums, :artist_id
    add_index :albums, :year
  end
end

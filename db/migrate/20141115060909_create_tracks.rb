class CreateTracks < ActiveRecord::Migration
  def change
    create_table :tracks do |t|
      t.string :title
      t.integer :artist_id
      t.timestamps
    end
    add_index :tracks, :artist_id
  end
end

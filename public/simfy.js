$(function(){
	$('body').on('blur', '#album_title, #artist_id', function(){
		album_title = $('#album_title').val();
		artist_title = $('#artist_id :selected').text();
		q = album_title + '+' + artist_title
		url = "https://itunes.apple.com/search?media=music&entity=album&term=" + q + "&limit=1";

		$.ajax({
			type: 'GET',
        	url: url,
        	dataType: 'JSONP'
    	}).done(function(data){
		  	var album = data.results[0];
		  	var d = new Date(data.results[0].releaseDate);
		  	$('#fetched-cover').prop('src', album.artworkUrl100)
		  	$('#album_year').val(d.getFullYear());
		  	$.ajax({
				type: 'GET',
	        	url: "https://itunes.apple.com/lookup?id=" + data.results[0].collectionId + "&entity=song",
	        	dataType: 'JSONP'
	    	}).done(function(data){
	    		$('#tracklist').html('');
	    		$.each( data.results, function( i, track ) {
	    			if(track.wrapperType == 'track'){
	    				$('#tracklist').append('<div class="row"><div class="col-xs-1"><a href="#" class="removeTrack"><i class="glyphicon glyphicon-remove-circle"></i></a></div><div class="col-xs-11"><input name="tracks[][title]" class="trackinput form-control" value="' + track.trackName + '" /></div></div>');
	    			}
				});
	    	});
		  	
    	});

	})

	$('body').on('click', '.removeTrack', function(){
		$(this).parents('.row').remove();
	});
})